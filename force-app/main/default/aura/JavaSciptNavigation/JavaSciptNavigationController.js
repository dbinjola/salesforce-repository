({
    //Open URL in New Browser Tab
    handleOpenInNewWindow : function(component, event, helper) {
       // window.open("<a class="" href="https://www.salesforce.com" rel="nofollow"><span>https</span><span>://</span><span>www</span><span>.</span><span>salesforce</span><span>.</span><span>com</span></a>", '_blank');
    },
     
    //Open URL in New Browser Tab With Record Id
    handleOpenNewWindowWithRecordId : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        window.open('/' + recordId,'_blank');
    }
})